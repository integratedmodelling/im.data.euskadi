app urdaibai.en
	"ARIES web app for Urdaibai by BC3"
	description "This application demonstrates some basic data and modelling workflows, building on GeoEuskadi information, for the Urdaibai Biosphere Reserve"
	logo static/urdaibai_logo_transparent_small.png
	versionstring "Demo"
	style light
	locale en-US
	
action init:
	context(:reset)
	set observations_selected ()
	initvar
	
action initvar:
	observations_selected.clear
	set self.context_type 'historic'
	set self.context_temporal_label "Historical context (1991 - 2006)"
	set self.context_spatial_label "20 m"
	set self.watershed "Murueta watershed"
		
@left(width=350)	
action main:	
	(	
		separator("Context selection" :iconname "clock-time-four-outline" #urdaibai_sep)
		label("" #label1)
		(
			label("Temporal context:" width="98%" #ltempres)
			combo(
				"Historical context (1991 - 2006)"
				"Current context"
				"Future scenarios (2050 - 2101)"
				#ctempres
				width="95%"
			): (
				set self.context_temporal_label $
				if [self.context_temporal_label.contains('Historical')] (
					set self.context_type 'historic'
				)
				if [self.context_temporal_label.contains('Current')] (
					set self.context_type 'present'
				)
				if [self.context_temporal_label.contains('Future')] (
					set self.context_type 'future'
				)
			)
			
		) :vbox :hfill
		label("" #label2)
		label("" #label3)
		(
			label("Spatial resolution:" width="68%" #lspatres)
			combo(
					"20 m"
					"50 m" 
					"100 m" 
					#cspatres
					width="25%"
				): 
				(
					set self.context_spatial_label $
				)
		) :hbox
		(
			label("" #lcontext width="95%" :hidden)
		)
	) 
	(
		button(#bcontext "Set context" :iconname "earth" :tooltip "This context is predefined for the whole of the Urdaibai Biosphere Reserve."): (
			if [self.context_type == 'historic'] (
				if [self.context_spatial_label == "20 m"]
					context(geoeuskadi.geography.urdaibai_historic_20m)
				if [self.context_spatial_label == "50 m"]
					context(geoeuskadi.geography.urdaibai_historic_50m)
				if [self.context_spatial_label == "100 m"]
					context(geoeuskadi.geography.urdaibai_historic_100m)
			)
			if [self.context_type == 'present'] (
				if [self.context_spatial_label == "20 m"]
					context(geoeuskadi.geography.urdaibai_present_20m)
				if [self.context_spatial_label == "50 m"]
					context(geoeuskadi.geography.urdaibai_present_50m)
				if [self.context_spatial_label == "100 m"]
					context(geoeuskadi.geography.urdaibai_present_100m)
			)
			if [self.context_type == 'future'] (
				if [self.context_spatial_label == "20 m"]
					context(geoeuskadi.geography.urdaibai_future_20m)
				if [self.context_spatial_label == "50 m"]
					context(geoeuskadi.geography.urdaibai_future_50m)
				if [self.context_spatial_label == "100 m"]
					context(geoeuskadi.geography.urdaibai_future_100m)
			)
			ctempres.hide
			ltempres.hide
			lcontext.show
			lcontext.update([self.context_temporal_label + " at " + self.context_spatial_label])
			lspatres.hide
			cspatres.hide
			queries.show
			breset.show
			bcontext.hide
			
			//These are the Hydrology part components that are going to be shown or
			//hidden when setting the General Urdaibai context
			hydro_sep.hide
			hydro_tempres.hide
			htempres.hide
			hydrology_context.hide
			label4.hide
			label5.hide
			hcontext.hide

		)
		button(#breset "Reset context" :iconname "restart" :hidden): (
			context(:reset)
			ltempres.show
			ctempres.reset
			ctempres.show
			lcontext.hide
			lspatres.show
			cspatres.reset
			cspatres.show
			queries.hide
			breset.hide
			bcontext.show
			observations_selected.clear
			observations_separator.hide
			observations_group.reset
			initvar
			
			//These are the Hydrology part components that are going to be shown or
			//hidden when setting the General Urdaibai context
			hydro_sep.show
			hydro_tempres.show
			htempres.show
			hydrology_context.show
			label4.show
			label5.show
			hcontext.show

			//
			hydrology_context.hide
		)		
		
		button("Queries" #queries :iconname "help" :hidden): geography
	) :hbox :hfill

	
//	
	separator("Hydrology" :iconname "earth" #hydro_sep)
		label("")
		(
			label("Watershed selection:" width="98%" #hydro_tempres)
			combo(  //"Oka watershed"
					"Murueta watershed"
					//"Mape watershed"
					//"Bermeo north watershed"
					"Bermeo south watershed"
					"Puerto Bermeo watershed"
					"Puerto Mundaka watershed"
					"Mundaka watershed"
					"Antzora watershed"
					"Azalene watershed"
					//"Laga watershed"
					"Artzuela watershed"
					"Mape upstream watershed"
					#htempres
					width="95%"
					): (set self.watershed $)
	
		):vbox :hfill
		
		(
			label("" #hydrology_context width="95%" :hidden)
		)
		
		label("" #label4)
		label("" #label5)
		
		(
		button(#hcontext "Set watershed" :iconname "wave" :tooltip "This context is predefined only for the selected watershed."): (
			hydrology_context.show
			if [self.watershed == 'Oka watershed'] (
				context(geoeuskadi.geography.oka_watershed_present_50m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 50 meters"])
			)
			if [self.watershed == 'Murueta watershed'] (
				context(geoeuskadi.geography.murueta_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 5 meters"])
			)
			if [self.watershed == 'Mape watershed'] (
				context(geoeuskadi.geography.mape_all_watershed_present_25m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 25 meters"])
			)
			if [self.watershed == 'Bermeo north watershed'] (
				context(geoeuskadi.geography.bermeo1_watershed_present_25m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 25 meters"])
			)
			if [self.watershed == 'Bermeo south watershed'] (
				context(geoeuskadi.geography.bermeo2_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 5 meters"])
			)
			if [self.watershed == 'Puerto Bermeo watershed'] (
				context(geoeuskadi.geography.puerto_bermeo_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 5 meters"])
			)
			if [self.watershed == 'Puerto Mundaka watershed'] (
				context(geoeuskadi.geography.puerto_mundaka_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 5 meters"])
			)
			if [self.watershed == 'Mundaka watershed'] (
				context(geoeuskadi.geography.mundaka_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 5 meters"])
			)
			if [self.watershed == 'Antzora watershed'] (
				context(geoeuskadi.geography.antzora_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 2 meters"])
			)
			if [self.watershed == 'Azalene watershed'] (
				context(geoeuskadi.geography.azalene_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 5 meters"])
			)
			if [self.watershed == 'Laga watershed'] (
				context(geoeuskadi.geography.laga_watershed_present_25m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 25 meters"])
			)
			if [self.watershed == 'Artzuela watershed'] (
				context(geoeuskadi.geography.artzuela_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 1 meters"])
			)
			if [self.watershed == 'Mape upstream watershed'] (
				context(geoeuskadi.geography.mape_top_watershed_present_5m)
				hydrology_context.update([self.watershed + " with a spatial resolution of 2 meters"])
			)
			
			hydro_queries.show
			hcontext.hide
			htempres.hide
			reset2.show
			
			//These are the components of the General Urdaibai part that will be hidden or shown 
			//when setting the watershets
			ltempres.hide
			ctempres.hide
			lspatres.hide
			cspatres.hide
			lcontext.hide
			bcontext.hide
			urdaibai_sep.hide
			label1.hide
			label2.hide
			label3.hide
		)
		
		button(#reset2 "Reset" :iconname "restart" :hidden):(
			context(:reset)
			hydrology_context.hide
			hydro_queries.hide
			hcontext.show
			htempres.show
			reset2.hide
			set self.watershed "Artzuela watershed"	
			
			//These are the components of the General Urdaibai part that will be hidden or shown 
			//when setting the watershets
			ltempres.show
			ctempres.show
			lspatres.show
			cspatres.show
			lcontext.show
			bcontext.show
			urdaibai_sep.show
			label1.show
			label2.show
			label3.show
			observations_selected.clear
			observations_separator.hide
			observations_group.reset
			
			//
			lcontext.hide
		)
		
		button(#hydro_queries "Hydrology queries" :iconname "help" :hidden): hydrology
		
		):hbox :hfill

	
	separator("Selected observations" #observations_separator :hidden)
	(
	) :hfill #observations_group
	
	(
		separator("Information websites" :iconname "information-outline")
		(
			button ("Urdaibai" :iconname "pine-tree"): urdaibai
			button("ARIES" :iconname "server-network"): aries
		) :hbox
		(
			button("Userguide" :iconname "cellphone-information"): userguide
			button("IHOBE" :iconname "sprout"): ihobe
		):hbox
	):hfill :bottom

@modal(width=700, height=480, title="Select the desired query")
action geography:
	separator("Geography")
	%%%
	%%%
	(
		checkbutton("Elevation" #elevation :disabled [observations_selected.contains("elevation") || self.context_type != 'present']): (
			addobservation("elevation", new observation.urdaibai({geography:Elevation in m named elevation}, "Elevation", "image-filter-hdr", true))
			elevation.disable
		)
		label(:iconname "image-filter-hdr" :width "col1")
	) :hbox
	(
		checkbutton("Aspect" #aspect :disabled [observations_selected.contains("aspect") || self.context_type != 'present']): (
			addobservation("aspect", new observation.urdaibai({geography:Aspect in degree_angle named aspect}, "Aspect", "image-filter-hdr", true))
			aspect.disable
		)
		label(:iconname "image-filter-hdr" :width "col1")
	) :hbox
	(
		checkbutton("Slope" #slope :disabled [observations_selected.contains("slope") || self.context_type != 'present']): (
			addobservation("slope", new observation.urdaibai({geography:Slope in degree_angle named slope}, "Slope", "image-filter-hdr", true))
			slope.disable
		)
		label(:iconname "image-filter-hdr" :width "col1")
	) :hbox
	(
		checkbutton("Bathymetry" #bathymetry :disabled [observations_selected.contains("bathymetry") || self.context_type != 'present']): (
			addobservation("bathymetry", new observation.urdaibai({geography:BathymetricDepth in m named bathymetry}, "Bathymetry", "waves", true))
			bathymetry.disable
		)
		label(:iconname "waves" :width "col1")
	) :hbox
	(
		checkbutton("Distance to coastline" #distance_to_coastline :disabled [observations_selected.contains("distance_to_coastline") || self.context_type != 'present']): (
			addobservation("distance_to_coastline", new observation.urdaibai({distance to geoeuskadi.geography:Urdaibai earth:Coastline in m}, "Distance to coastline", "island", true))
			distance_to_coastline.disable
		)
		label(:iconname "island" :width "col1")	
	) :hbox
	
	(
		checkbutton("Land Cover" #land_cover :disabled [observations_selected.contains("land_cover") || (self.context_type != 'historic' && self.context_type != 'present')]): (
			addobservation("land_cover", new observation.urdaibai({landcover:LandCoverType}, "Land Cover", "pine-tree", true))
			land_cover.disable	
		)
		label(:iconname "pine-tree" :width "col1")	
	) :hbox
	(
		checkbutton("Land account" #land_account :disabled [observations_selected.contains("land_account") || self.context_type != 'present']): (
			addobservation("land_account", new observation.urdaibai(geoeuskadi.landcover.present_land_cover, "Land account", "table-multiple", true))
			land_account.disable	
		)
		label(:iconname "table-multiple" :width "col1")	
	) :hbox
	(
		checkbutton("Land account: net balance" #land_account_net_balance :disabled [observations_selected.contains("land_account_net_balance") || self.context_type != 'historic']): (
			addobservation("land_account_net_balance", new observation.urdaibai(seea.aries.tables.landcover.landcover_basic, "Land account: net balance", "table-multiple", true))
			land_account_net_balance.disable	
		)
		label(:iconname "table-multiple" :width "col1")	
	) :hbox
	
	//create observation.urdaibai({magnitude of geography:BathymetricDepth for agriculture:Mariculture}, "Depth suitability for mariculture", "fish")
	label("")
	(
		button("Geography and Land Cover" :iconname "image-filter-hdr" :disabled)
		button("Soil":iconname "pine-tree"): soil
		button("Saltmarshes" :iconname "sprout"): saltmarshes
	):hbox
	
@modal(width=700, height=480, title="Select the desired query")
action soil:
	separator("Soil and Land Cover")
	%%%
	%%%
	(
		checkbutton("Soil organic carbon" #soil_organic_carbon :disabled [observations_selected.contains("soil_organic_carbon") || self.context_type != 'present']): (
			addobservation("soil_organic_carbon", new observation.urdaibai({soil:Soil chemistry:Organic chemistry:Carbon im:Mass in t/ha}, "Soil organic carbon", "molecule-co2", true))
			soil_organic_carbon.disable	
		)
		label(:iconname "molecule-co2" :width "col1")
	) :hbox
	(
		checkbutton("Percentage of Clay" #percentage_of_clay :disabled [observations_selected.contains("percentage_of_clay") || self.context_type != 'present']): (
			addobservation("percentage_of_clay", new observation.urdaibai({percentage of soil:Clay in soil:TopSoil im:Volume}, "Percentage of Clay", "division", true))
			percentage_of_clay.disable	
		)
		label(:iconname "division" :width "col1")	
	) :hbox
	(
		checkbutton("Percentage of Sand" #percentage_of_sand :disabled [observations_selected.contains("percentage_of_sand") || self.context_type != 'present']): (
			addobservation("percentage_of_sand", new observation.urdaibai({percentage of soil:Sand in soil:TopSoil im:Volume}, "Percentage of Sand", "division", true))
			percentage_of_sand.disable	
		)
		label(:iconname "division" :width "col1")	
	) :hbox
	(
		checkbutton("Percentage of Silt" #percentage_of_silt :disabled [observations_selected.contains("percentage_of_silt") || self.context_type != 'present']): (
			addobservation("percentage_of_silt", new observation.urdaibai({percentage of soil:Silt in soil:TopSoil im:Volume}, "Percentage of Silt", "division", true))
			percentage_of_silt.disable	
		)
		label(:iconname "division" :width "col1")	
	) :hbox
	(
		checkbutton("Lithology" #lithology :disabled [observations_selected.contains("lithology") || self.context_type != 'present']): (
			addobservation("lithology", new observation.urdaibai({soil:Soil im:Type}, "Lithology", "information-variant", true))
			lithology.disable	
		)
		label(:iconname "information-variant" :width "col1")	
	) :hbox
	
	(
		checkbutton("Potential removed soil mass" #removed_soil :disabled [observations_selected.contains("removed_soil") || (self.context_type != 'historic' && self.context_type != 'present')]): (
			addobservation("removed_soil", new observation.urdaibai({im:Potential (im:Removed soil:Soil im:Mass) in t/ha}, "Potential removed soil mass", "excavator", true))
			removed_soil.disable	
		)
		label(:iconname "excavator" :width "col1")	
	) :hbox
	
	(
		checkbutton("Reteined soil mass from vegetation in tones per ha" #reteined_soil_veg :disabled [observations_selected.contains("retained_soil") || (self.context_type != 'historic' && self.context_type != 'present')]): (
			addobservation("retained_soil", new observation.urdaibai({im:Retained soil:Soil im:Mass caused by ecology:Vegetation in t/ha}, "Reteined soil mass by vegetation", "tree", true))
			reteined_soil_veg.disable	
		)
		label(:iconname "tree" :width "col1")	
	) :hbox
	
	label("")
	(
		button("Geography and Land Cover" :iconname "image-filter-hdr"): geography
		button("Soil":iconname "pine-tree" :disabled)
		button("Saltmarshes" :iconname "sprout"): saltmarshes
	):hbox			
@modal(width=700, height=480, title="Select the desired query")
action saltmarshes:
	separator("Saltmarshes" )
	%%%
	%%%
	(
		checkbutton("Saltmarsh distribution" #saltmarsh_distribution :disabled [observations_selected.contains("saltmarsh_distribution") || self.context_type != 'present']): (
			addobservation("saltmarsh_distribution", new observation.urdaibai ({type of geoeuskadi.landcover:Marsh named actual_2017_saltmarshes}, "Saltmarsh distribution", "sprout", true))
			saltmarsh_distribution.disable	
		)
		label(:iconname "sprout" :width "col1")	
	) :hbox
	(
		checkbutton("Saltmarsh potential distribution in RCP4.5" #saltmarsh_distribution_rcp45 :disabled [observations_selected.contains("saltmarsh_distribution_rcp45") || self.context_type != 'future']): (
			addobservation("saltmarsh_distribution_rcp45", new observation.urdaibai ({type of geoeuskadi.landcover:Marsh caused by geoeuskadi.landcover:Rcp4_5 named saltmarshes_rcp45}, "Saltmarsh potential distribution in RCP4.5", "waves", true))
			saltmarsh_distribution_rcp45.disable	
		)
		label(:iconname "waves" :width "col1")	
	) :hbox
	(
		checkbutton("Saltmarsh potential distribution in RCP8.5" #saltmarsh_distribution_rcp85 :disabled [observations_selected.contains("saltmarsh_distribution_rcp85") || self.context_type != 'future']): (
			addobservation("saltmarsh_distribution_rcp85", new observation.urdaibai ({type of geoeuskadi.landcover:Marsh caused by geoeuskadi.landcover:Rcp8_5 named saltmarshes_rcp85}, "Saltmarshes potential distribution in RCP8.5", "waves", true))
			saltmarsh_distribution_rcp85.disable	
		)
		label(:iconname "waves" :width "col1")	
	) :hbox
	(
		checkbutton("Actual saltmarsh area" #actual_saltmarsh_area :disabled [observations_selected.contains("actual_saltmarsh_area") || self.context_type != 'present']): (
			addobservation("actual_saltmarsh_area", new aries.components.selectors.observation (geoeuskadi.landcover.saltmarsh_2017, "Actual saltmarsh area", :iconname "table-multiple", true))
			//addobservation("actual_saltmarsh_area", new observation.urdaibai (geoeuskadi.landcover.saltmarsh_2017, "Actual saltmarsh area", "table-multiple", true))
			actual_saltmarsh_area.disable	
		)
		label(:iconname "table-multiple" :width "col1")	
	) :hbox
	(
		checkbutton("Saltmarsh net change: RCP4.5" #saltmarsh_net_change_rcp45 :disabled [observations_selected.contains("saltmarsh_net_change_rcp45") || self.context_type != 'future']): (
			addobservation("saltmarsh_net_change_rcp45", new aries.components.selectors.observation (geoeuskadi.landcover.saltmarshes_basic_rcp45, "Saltmarsh net change: RCP4.5", :iconname "table-multiple", true))
			saltmarsh_net_change_rcp45.disable	
		)
		label(:iconname "table-multiple" :width "col1")	
	) :hbox
	(
		checkbutton("Saltmarsh net change: RCP8.5" #saltmarsh_net_change_rcp85 :disabled [observations_selected.contains("saltmarsh_net_change_rcp85") || self.context_type != 'future']): (
			addobservation("saltmarsh_net_change_rcp85", new aries.components.selectors.observation (geoeuskadi.landcover.saltmarshes_basic_rcp85, "Saltmarsh net change: RCP8.5", :iconname "table-multiple", true))
			saltmarsh_net_change_rcp85.disable	
		)
		label(:iconname "table-multiple" :width "col1")	
	) :hbox
	(
		checkbutton("Actual saltmarsh ecosystem services" #actual_saltmarsh_ecosystem_services :disabled [observations_selected.contains("actual_saltmarsh_ecosystem_services") || self.context_type != 'present']): (
			addobservation("actual_saltmarsh_ecosystem_services", new observation.urdaibai (geoeuskadi.mars.ecosystem.services.tables.marsh_ecosystem_service_value_2017, "Actual saltmarsh ecosystem services", "calculator", true))
			actual_saltmarsh_ecosystem_services.disable	
		)
		label(:iconname "calculator" :width "col1")	
	) :hbox
	(
		checkbutton("Saltmarsh ecosystem services values. Net change: RCP4.5" #actual_saltmarsh_ecosystem_services_rcp45 :disabled [observations_selected.contains("actual_saltmarsh_ecosystem_services_rcp45") || self.context_type != 'future']): (
			addobservation("actual_saltmarsh_ecosystem_services_rcp45", new observation.urdaibai (geoeuskadi.mars.ecosystem.services.tables.marsh_ecosystem_service_value_dynamic_rcp4_5, "Saltmarsh ecosystem services values. Net change: RCP4.5", "calculator", true))
			actual_saltmarsh_ecosystem_services_rcp45.disable	
		)
		label(:iconname "calculator" :width "col1")	
	) :hbox
	(
		checkbutton("Saltmarsh ecosystem services values. Net change: RCP8.5" #actual_saltmarsh_ecosystem_services_rcp85 :disabled [observations_selected.contains("actual_saltmarsh_ecosystem_services_rcp85") || self.context_type != 'future']): (
			addobservation("actual_saltmarsh_ecosystem_services_rcp85", new observation.urdaibai (geoeuskadi.mars.ecosystem.services.tables.marsh_ecosystem_service_value_dynamic_rcp8_5, "Saltmarsh ecosystem services values. Net change: RCP8.5", "calculator", true))
			actual_saltmarsh_ecosystem_services_rcp85.disable	
		)
		label(:iconname "calculator" :width "col1")	
	) :hbox
    	 
	(
		button("Geography and Land Cover" :iconname "image-filter-hdr"): geography
		button("Soil":iconname "pine-tree"): soil
		button("Saltmarshes" :iconname "sprout" :disabled)
	):hbox
	
	
@modal(width=700, height=480, title="Select the desired query")
action hydrology:
	separator("Hydrology")
	%%%
	%%%
	(
		checkbutton("Runnoff estimates" #runoff :iconname "slope-downhill"): (
//			addobservation("Watershed", new observation.urdaibai(geoeuskadi.hydrology.full_urdaibai_watershed, "Watershed", "slope-downhill", true))
//			addobservation("Streams", new observation.urdaibai(geoeuskadi.hydrology.urdaibai_streams, "Streams", "slope-downhill", true))
			addobservation("Runoff", new observation.urdaibai(geoeuskadi.hydrology.urdaibai_runoff, "Runoff", "slope-downhill", true))
			runoff.disable
		)
		label(:iconname "slope-downhill" :width "col1")
	) :hbox
	
	
	
action addobservation(index, obs):
	if [!observations_selected.contains(index)] (
		observations_selected.add(index)
		observations_separator.show
		observations_group.add(obs)
	)
	
//action runoff: 
//	observe({hydrology:Runoff within hydrology:HydrologicallyCorrected hydrology:Watershed})

@modal(width=700, height=640, title="How to use ARIES for Urdaibai")
action userguide:
	html("https://integratedmodelling.org/statics/im.data.euskadi/en/")

@modal(width=700, height=640, title="IHOBE website")
action ihobe:
	html("https://www.ihobe.eus/home")
	
@modal(width=700, height=640, title="Urdaibai website")
action urdaibai:
	html("https://www.urdaibai.eus/en/")
	
@modal(width=700, height=640, title="ARIES website")
action aries:
	html("https://aries.integratedmodelling.org/")
